import React, {Component} from 'react';
import {ToolbarAndroid, StyleSheet} from 'react-native';

export default class Toolbar extends Component {
	render() {
  return (
     <ToolbarAndroid
            style={styles.toolbar}
            title="Movies"
            onActionSelected={this.onActionSelected}
            titleColor= "#000"
            actions = {[
              {title: "Log out", show: "never"}
            ]}
            />
  )
}
 onActionSelected(position) {
     }
}

const styles= StyleSheet.create ({

	 toolbar: {
   backgroundColor: '#2196F3',
   height: 56,
   alignSelf: 'stretch',
 }, 
})
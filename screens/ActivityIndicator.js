import React, { Component } from 'react'
import {
  ActivityIndicator,
  AppRegistry,
  StyleSheet,
  Text,
  View,
} from 'react-native'

export default class App extends Component {
  render() {
    return (
     <View style={[styles.container]}>
      <View  style={[styles.head]}>
        <Text style={{fontSize:25}}>Activity Indicator</Text>
      </View>
       <View style={[styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
        <ActivityIndicator size="small" color="#00ff00" />
        <ActivityIndicator size="large" color="#0000ff" />
        <ActivityIndicator size="small" color="#00ff00" />
      </View>
     </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  head: {
    alignItems: 'center',
    justifyContent: 'center',
    height : 100,
    margin: 50,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})

AppRegistry.registerComponent('App', () => App)

import React, { Component } from 'react'
import { View, Switch, StyleSheet } from 'react-native'

export default class SwitchV extends Component{
 render() {
     return (
      <View style = {styles.container}>
         <Switch/>    
      </View>
   )
 }
}
const styles = StyleSheet.create ({
   container: {
      flex: 1,
      alignItems: 'center',
      marginTop: 100
   }
})
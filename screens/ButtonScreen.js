import React, { Component } from 'react';
import {StyleSheet,Text, View, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation';


export default class App extends Component {
  render() {
    return (
     <View style={[styles.container]}>
      <View  style={[styles.head]}>
        <Text style={{fontSize:25}}>Button</Text>
      </View>
       <View style={[styles.content1]}>
       	<Button title="back" onPress={() => this.props.navigation.goBack()}/>
       
      </View>
     </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  head: {
    alignItems: 'center',
    justifyContent: 'center',
    height : 100,
    margin: 50,
  },
  Content1: {
  	alignItems: 'center',
    justifyContent: 'center',
  }
})


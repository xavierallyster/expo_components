import React, { Component } from 'react';
import {ScrollView, RefreshControl,Text} from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class RefreshableList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.setState({refreshing: false});
   
  }

  render() {
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
      >
      <Text style={{margin: 140}}>Swipe Down</Text>
      </ScrollView>
    );
  }
}
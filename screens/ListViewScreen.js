import React, {Component} from 'react';
import {ListView, Text, StyleSheet, View} from 'react-native';
import {createStackNavigator} from 'react-navigation';




export default class App extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(['Dog', 'cat', 'horse', 'rat', 'bird', 'pig', 'tiger', 'cow']),
    };
  }

  render() {
    return (
    	<View style={styles.SList}>
    		<ListView
        		dataSource={this.state.dataSource}
        		initialListSize={5}
        		renderRow={(rowData) => <Text>{rowData}</Text>}
      		/>
    	</View>
    );
  }
}

const styles= StyleSheet.create({
	SList: {
		alignItems: 'center',
		margin: 20,
	}
})
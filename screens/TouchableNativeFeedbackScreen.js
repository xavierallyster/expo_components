import React, {Component} from 'react';
import {TouchableNativeFeedback, View, Text} from 'react-native';

export default class touchablenativefeedbackScreen extends Component {
	render() {
  return (
    <TouchableNativeFeedback
        onPress={this._onPressButton}
        background={TouchableNativeFeedback.SelectableBackground()}>
      <View style={{width: 150, height: 100, backgroundColor: 'red'}}>
        <Text style={{margin: 30}}>Button</Text>
      </View>
    </TouchableNativeFeedback>
  );
}
}
import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, Button } from 'react-native';

export default class HomeScreen extends Component {
    render() {
        return (
          <ScrollView style={styles.pad}>
          <Button title="Activity Indicator" onPress={() => this.props.navigation.navigate('ActivityIndicator')}/>
          <Button title="Button" onPress={() => this.props.navigation.navigate('ButtonScreen')}/>
          <Button title="Date Picker (IOS)" onPress={() => this.props.navigation.navigate('DatepickerScreen')}/>
          <Button title="Drawer layout" onPress={() => this.props.navigation.navigate('DrawerlayoutScreen')}/>
          <Button title="Flat List" onPress={() => this.props.navigation.navigate('FlatlistScreen')}/>
          <Button title="Image" onPress={() => this.props.navigation.navigate('ImageScreen')}/>
          <Button title="Keyboard Avoiding View" onPress={() => this.props.navigation.navigate('KeyboardAvoidingViewScreen')}/>
          <Button title="List View" onPress={() => this.props.navigation.navigate('ListViewScreen')}/>
          <Button title="Modal" onPress={() => this.props.navigation.navigate('ModalScreen')}/>
          <Button title="Picker" onPress={() => this.props.navigation.navigate('PickerScreen')}/>
          <Button title="Progress Bar Android" onPress={() => this.props.navigation.navigate('ProgressBarAndroidScreen')}/>
          <Button title="Refresh Control" onPress={() => this.props.navigation.navigate('RefreshControlScreen')}/>
          <Button title="Safe Area View" onPress={() => this.props.navigation.navigate('SafeAreaViewScreen')}/>
          <Button title="Scroll View" onPress={() => this.props.navigation.navigate('ScrollViewScreen')}/>
          <Button title="Section List" onPress={() => this.props.navigation.navigate('SectionListScreen')}/>
          <Button title="Slider" onPress={() => this.props.navigation.navigate('SliderScreen')}/>
          <Button title="Status Bar" onPress={() => this.props.navigation.navigate('StatusBarScreen')}/>
          <Button title="Switch" onPress={() => this.props.navigation.navigate('SwitchScreen')}/>
          <Button title="Text" onPress={() => this.props.navigation.navigate('TextScreen')}/>
          <Button title="Text Input" onPress={() => this.props.navigation.navigate('TextInputScreen')}/>
          <Button title="Toolbar Android" onPress={() => this.props.navigation.navigate('ToolbarAndroidScreen')}/>
          <Button title="Touchable Highlight" onPress={() => this.props.navigation.navigate('TouchableHighlightScreen')}/>
          <Button title="Touchable Native Feedback" onPress={() => this.props.navigation.navigate('TouchableNativeFeedbackScreen')}/>
          <Button title="Touchable Opacity" onPress={() => this.props.navigation.navigate('TouchableOpacityScreen')}/>
          <Button title="View" onPress={() => this.props.navigation.navigate('ViewScreen')}/>
          <Button title="View Pager Android" onPress={() => this.props.navigation.navigate('ViewPagerAndroidScreen')}/>
          <Button title="Web View" onPress={() => this.props.navigation.navigate('WebViewScreen')}/>
          </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flexDirection: 'column',
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'space-around'
    },

    pad: {
      padding: 10

    }
  });
  

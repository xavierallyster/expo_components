import React, { Component } from 'react';
import {SafeAreaView, View, Text} from 'react-native';
import {createStackNavigator} from 'react-navigation';

export default class SafeAreaViewScreen extends Component {
	render() {
		return (
			<SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
  				<View style={{flex: 1}}>
    				<Text>Hello World!</Text>
  				</View>
			</SafeAreaView>
		)
	}

}
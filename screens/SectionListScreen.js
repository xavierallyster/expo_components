import React, { Component } from 'react';
import {Text, SectionList} from 'react-native';


export default class SectionL extends Component {
	render() {
		return (
			<SectionList
  				renderItem={({item, index, section}) => <Text key={index}>{item}</Text>}
  				renderSectionHeader={({section: {title}}) => (
    				<Text style={{fontWeight: 'bold'}}>{title}</Text>
  				)}
  				sections={[
				    {title: 'Fruits', data: ['Apple', 'Banana', 'Cherry', 'Dalandan',]},
				    {title: 'Car Brands', data: ['Toyota', 'Nissan', 'Audi', 'Ford']},
				    {title: 'Animals', data: ['Cat', 'Dog', 'Chicken', 'Snake', 'Tiger', 'lion']},
  				]}
  				keyExtractor={(item, index) => item + index}
			/>
		);
	}
}
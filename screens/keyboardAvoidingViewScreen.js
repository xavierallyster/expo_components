import React, { Component } from 'react';
import { KeyboardAvoidingView ,StyleSheet, TextInput, View} from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class App extends Component {

  render () {
    return (
      
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View>
          <TextInput style={{height: 40}} placeholder="Type here to translate!"  />
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,

  },
})


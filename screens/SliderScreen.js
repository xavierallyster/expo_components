import React, {Component} from 'react';
import {Slider, StyleSheet} from 'react-native';

export default class Slide extends Component {

	render() {
		return (
			<Slider
            Style={styles.track}
            minimumTrackTintColor='#2f2f2f'
          />
		);
	}
}

const styles= StyleSheet.create({
	 track: {
    height: 1,
    backgroundColor: '#303030',
   	marginTop: 100,
  },

})
import React from 'react';
import { StyleSheet, View, Text, Button} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen';
import DetailsScreen from './screens/DetailsScreen';
import ActivityIndicator from './screens/ActivityIndicator';
import ButtonScreen from './screens/ButtonScreen';
import DatepickerScreen from './screens/DatepickerScreen';
import DrawerlayoutScreen from './screens/DrawerlayoutScreen';
import FlatlistScreen from './screens/FlatlistScreen';
import ImageScreen from './screens/ImageScreen';
import KeyboardAvoidingViewScreen from './screens/keyboardAvoidingViewScreen';
import ListViewScreen from './screens/ListViewScreen';
import ModalScreen from './screens/ModalScreen';
import PickerScreen from './screens/PickerScreen';
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen';
import RefreshControlScreen from './screens/RefreshControlScreen';
import SafeAreaViewScreen from './screens/SafeAreaViewScreen';
import ScrollViewScreen from './screens/ScrollViewScreen';
import SectionListScreen from './screens/SectionListScreen';
import SliderScreen from './screens/SliderScreen';
import StatusBarScreen from './screens/StatusBarScreen';
import SwitchScreen from './screens/SwitchScreen';
import TextScreen from './screens/TextScreen';
import TextInputScreen from './screens/TextInputScreen';
import ToolbarAndroidScreen from  './screens/ToolbarAndroidScreen';
import TouchableHighlightScreen from './screens/TouchableHighlightScreen';
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen';
import TouchableOpacityScreen from './screens/TouchableOpacityScreen';
import ViewScreen from './screens/ViewScreen';
import ViewPagerAndroidScreen from './screens/ViewPagerAndroidScreen';
import WebViewScreen from './screens/WebViewScreen';


const RootStack = createStackNavigator(
  {

    Home: HomeScreen,

    Details: DetailsScreen,

    ActivityIndicator: ActivityIndicator,

    ButtonScreen: ButtonScreen,

    DatepickerScreen: DatepickerScreen,

    DrawerlayoutScreen: DrawerlayoutScreen,

    FlatlistScreen: FlatlistScreen,

    ImageScreen: ImageScreen,

    KeyboardAvoidingViewScreen: KeyboardAvoidingViewScreen,

    ListViewScreen: ListViewScreen,

    ModalScreen: ModalScreen,
    
    PickerScreen: PickerScreen,

    ProgressBarAndroidScreen: ProgressBarAndroidScreen,

    RefreshControlScreen: RefreshControlScreen,

    SafeAreaViewScreen: SafeAreaViewScreen,

    ScrollViewScreen: ScrollViewScreen,

    SectionListScreen: SectionListScreen,

    SliderScreen: SliderScreen,

    StatusBarScreen: StatusBarScreen,

    SwitchScreen: SwitchScreen,

    TextScreen: TextScreen,

    TextInputScreen: TextInputScreen,

    ToolbarAndroidScreen: ToolbarAndroidScreen,

    TouchableHighlightScreen: TouchableHighlightScreen,

    TouchableNativeFeedbackScreen: TouchableNativeFeedbackScreen,

    TouchableOpacityScreen: TouchableOpacityScreen,

    ViewScreen: ViewScreen,

    ViewPagerAndroidScreen: ViewPagerAndroidScreen,

    WebViewScreen: WebViewScreen,

  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack/>;
  }
}

